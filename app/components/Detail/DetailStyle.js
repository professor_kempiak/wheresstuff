import { StyleSheet } from 'react-native';

const DetailStyle = StyleSheet.create({
  margin: {
    margin: 20,
  },
  mapView: {
    flex: 1,
    height: 200,
  },
  buttonsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 20,
    marginBottom: 20
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 5,
  },
  openingHoursTitle: {
    fontWeight: 'bold'
  },
  buttonText: {
    color: '#FFFFFF',
    fontWeight: 'bold',
    fontSize: 11
  },
  dayCell: {
    color: 'grey',
    width: 100,
    alignSelf: 'flex-start'
  },
  hourCell: {
    alignSelf: 'flex-start'
  },
  openingHoursRow: {
    flex: 1,
    flexDirection: 'row',
    borderTopWidth: 1,
    borderColor: '#DDD'
  }
});

const NativeStyle = {
  button: {
    backgroundColor: '#ED5565',
    width: 130,
    justifyContent: 'center',
    alignItems: 'center',
  },
  left: {
    alignSelf: 'flex-start',
  },
  right: {
    alignSelf: 'flex-end',
  }
};

export { DetailStyle, NativeStyle };
