import { connect } from 'react-redux';
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import ReactNative from 'react-native';
import { Header, Button } from 'native-base';
import MapView from 'react-native-maps';
import Comm from 'react-native-communications';
//import { Actions } from 'react-native-router-flux';

import { ActionCreators } from '../../actions';
import { DetailStyle, NativeStyle } from './DetailStyle';


const {
  Text,
  View,
  ScrollView,
  Platform
} = ReactNative;

class Detail extends Component {

  componentDidMount() {
    this.props.findStore(this.props.placeId, this.props.locAddress);
  }

  launchDirection() {
    let url;
    if (Platform.OS === 'android') {
      url = `http://maps.google.com/maps?daddr=${this.props.location.latitude},${this.props.location.longitude}`;
    } else {
      url = `http://maps.apple.com/?daddr=${this.props.location.latitude},${this.props.location.longitude}`;
    }
    Comm.web(url);
  }


  render() {
    return (<View>
              <ScrollView>
                <MapView
                 ref={ref => { this.map = ref; }}
                 style={DetailStyle.mapView}
                 followUserLocation={false}
                 showsUserLocation={false}
                 zoomEnabled
                 pitchEnabled
                 animated={false}
                 cacheEnabled
                 loadingEnabled
                 region={{
                   latitude: this.props.location.latitude,
                   longitude: this.props.location.longitude,
                   latitudeDelta: 0.0042,
                   longitudeDelta: 0.0011,
                 }}
                >
                  <MapView.Marker
                    coordinate={this.props.location}
                    image={this.props.storeType.pins}
                  />
                </MapView>
                <View style={DetailStyle.margin}>
                  { this.props.storeDetail &&
                    <View>
                      <Text style={DetailStyle.title}>{this.props.storeDetail.name}</Text>
                      <Text>
                        {String(this.props.storeDetail.formatted_address).split(', ').join('\n')}
                      </Text>
                    </View>
                  }
                  <View style={DetailStyle.buttonsContainer}>
                    <Button
                      style={Object.assign({}, NativeStyle.button, NativeStyle.left)}
                      onPress={() => Comm.phonecall(this.props.storeDetail.formatted_phone_number, true)}
                    >
                      <Text style={DetailStyle.buttonText}>CALL</Text>
                    </Button>
                    <Button
                      style={Object.assign({}, NativeStyle.button, NativeStyle.right)}
                      onPress={() => this.launchDirection()}
                    >
                      <Text style={DetailStyle.buttonText}>GET DIRECTION</Text>
                    </Button>
                  </View>
                  <Text style={DetailStyle.openingHoursTitle}>Opening Hours</Text>
                  { !this.props.storeDetail.opening_days &&
                    <View style={[DetailStyle.openingHoursRow, { minHeight: 200 }]}>
                      <Text>No information yet.</Text>
                    </View>
                  }
                  { this.props.storeDetail.opening_days
                    && this.props.storeDetail.opening_days.map((dayText, index) => (
                    <View key={index} style={DetailStyle.openingHoursRow}>
                      <Text style={DetailStyle.dayCell}>{dayText.split(': ')[0]}</Text>
                      <Text style={DetailStyle.hourCell}>{dayText.split(': ')[1]}</Text>
                    </View>
                  ))}
                </View>
              </ScrollView>
            </View>);
  }
}

function mapStateToProps(state) {
  return { storeDetail: state.storeDetail };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Detail);
