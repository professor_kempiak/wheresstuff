import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import reducer from '../reducers';
import AppContainer from './AppContainer';
import { authDecorator } from '../services/authService';
import { NavigationActions } from 'react-navigation'

const loggerMiddleware = createLogger({
  predicate: () => __DEV__
});

function configureStore(initialState) {
  const enhancer = compose(
    applyMiddleware(
      thunkMiddleware, // lets us dispatch() functions
      loggerMiddleware,
    ),
  );
  return createStore(reducer, initialState, enhancer);
}

const store = configureStore({});

class Main extends React.Component {

  static navigationOptions = {
    headerMode: 'none',
    navigationOptions: {
        headerVisible: false,
    }
  }

  render(){
    return (
	  <Provider store={store}>
		  <AppContainer />
	  </Provider>
    )
  }
}

export default authDecorator(Main)
