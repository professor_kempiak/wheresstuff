import { connect } from 'react-redux';
import React, { Component } from 'react';
import MapView from 'react-native-maps';
import { bindActionCreators } from 'redux';
import { Actions } from 'react-native-router-flux';
import { ActionCreators } from '../../../actions';
import { getDefaultLocation } from '../../../services/api'

import Style from './MapStyle';

class Map extends Component {

  componentDidMount() {
    // navigator.geolocation.getCurrentPosition(
    //   (position) => { this.moveToPosition(position.coords); },
    //   (error) => alert(error.message),
    // );
    getDefaultLocation((defaultLocation) => {
      this.props.findStoreFromAddress(defaultLocation, this.props.currentStoreType);
    });
  }

  componentDidUpdate() {
    if (this.map && this.props.foundStores.length > 0) {
      this.map.fitToElements(true);
      setTimeout(() => { this.map.fitToElements(true); }, 2000);
    }
  }

  // moveToPosition(coords) {
  //   this.props.searchStores(coords, this.props.currentStoreType.ref);
  //   this.props.changeLocation(coords);
  // }

  loadStore(selectedStore) {
    Actions.detail({ ...selectedStore, storeType: this.props.currentStoreType });
  }

  render() {
    return (
        <MapView
          ref={ref => { this.map = ref; }}
          style={Style.mapView}
          followUserLocation={true}
          showsUserLocation={this.props.showUserLocation}
          zoomEnabled
          pitchEnabled
          animated
        >
          {this.props.foundStores.map((store, index) => (
            <MapView.Marker
              key={index}
              coordinate={store.location}
              image={this.props.currentStoreType.pins}
              onSelect={() => { this.loadStore(store); }}
              onPress={() => { this.loadStore(store); }}
            />
          ))}
        </MapView>
    );
  }
}

function mapStateToProps(state) {
  return {
    foundStores: state.foundStores,
    currentStoreType: state.currentStoreType,
    currentLocation: state.currentLocation,
    showUserLocation: state.showUserLocation
   };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Map);
