import { StyleSheet } from 'react-native';

const MapStyle = StyleSheet.create({
  mapView: {
    flex: 1
  }
});

export default MapStyle;
