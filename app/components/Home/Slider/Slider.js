import React, { Component } from 'react';
import ReactNative from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Button, Footer } from 'native-base';

import { ActionCreators } from '../../../actions';
import { SliderStyle, NativeStyle } from './SliderStyle';
import { storeTypes } from '../../../Config';

const {
  Platform,
  Animated,
  Text,
  TextInput,
  ScrollView,
  View,
  Image,
  TouchableHighlight
} = ReactNative;


const MaxHeight = Platform.OS === 'ios' ? 300 : 370;

class Slider extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      heightAnim: new Animated.Value(0),
      searchFocused: false,
      searchedText: '',
      currentText: ''
    };
  }

  toggleVisibility() {
    const VisibleStatus = !this.state.visible;
    Animated.timing(
       this.state.heightAnim,
       { toValue: (VisibleStatus ? MaxHeight : 0) }
    ).start();
    this.setState({ visible: VisibleStatus });
  }

  submitSearchAddress(text) {
    this.setState({ searchedText: text });
    setTimeout(() => { this.setState({ currentText: this.state.searchedText }); }, 400);
    this.hideKeyboard();
    this.toggleVisibility();
    if (text.length > 0) {
      return this.props.findStoreFromAddress(text, this.props.currentStoreType);
    }
    // this.moveToCurrentPosition();
  }

  // moveToCurrentPosition() {
  //   navigator.geolocation.getCurrentPosition(
  //     (position) => {
  //       this.props.changeShowUserLocation(true);
  //       this.props.searchStores(position.coords, this.props.currentStoreType.ref);
  //       this.props.changeLocation(position.coords);
  //     },
  //     (error) => alert(error.message),
  //   );
  // }

  updateStoreType(storeType) {
    this.props.searchStores(this.props.currentLocation, storeType.ref).then(() => {
      this.props.changeStoreType(storeType);
    })
    this.toggleVisibility();
  }

  hideKeyboard() {
    TextInput.State.blurTextInput(TextInput.State.currentlyFocusedField());
    this.setState({ searchFocused: false, currentText: this.state.searchedText });
  }

  showKeyboard() {
    this.setState({ searchFocused: true });
  }

  renderStoreType(storeType) {
    const style = SliderStyle.storeTypePicture;
    let focusStyle = SliderStyle.storeTypeFocused;
    if (storeType.ref === this.props.currentStoreType.ref) {
      focusStyle = SliderStyle.storeTypeNotFocused;
    }
    return (<Image source={storeType.filterImg} style={[style, focusStyle]} />);
  }

  renderSearchButton() {
    return (
    <View style={SliderStyle.slider}>
      <Button rounded style={NativeStyle.buttonSearch} onPress={() => { this.toggleVisibility(); }}>
        <Text style={SliderStyle.buttonText}>{this.state.visible ? 'Close' : 'Search'}</Text>
      </Button>
    </View>);
  }

  render() {
    return (<View>
        { this.renderSearchButton() }
        { this.state.searchFocused &&
          <View style={[SliderStyle.keyboardBar, { bottom: MaxHeight }]}>
            <TouchableHighlight
              activeOpacity={0.9}
              underlayColor="#eee"
              onPress={() => { this.hideKeyboard(); }}
            >
              <Text style={SliderStyle.keyboardBarButtonText}>Cancel</Text>
            </TouchableHighlight>
          </View>
        }
        <Animated.View style={[SliderStyle.animatedView, { height: this.state.heightAnim }]}>
          <Footer style={NativeStyle.footer}>
            <TextInput
              returnKeyType='go'
              placeholder="Specific location (ie: New York, NY 10118)"
              value={this.state.currentText}
              onChangeText={(text) => { this.setState({ currentText: text }); }}
              autoCorrect={false}
              onFocus={() => { this.showKeyboard(); }}
              onSubmitEditing={(event) => { this.submitSearchAddress(event.nativeEvent.text); }}
              style={SliderStyle.searchInput}
            />
            <View style={SliderStyle.title}>
              <Text style={SliderStyle.titleText}>PLACE TYPE</Text>
            </View>
            <ScrollView
              automaticallyAdjustInsets={false}
              horizontal
              decelerationRate={0}
              snapToAlignment="start"
              pagingEnabled
              showsHorizontalScrollIndicator={false}
            >
              {this.props.currentStoreType.ref && storeTypes.map((storeType, index) => (
                <View key={index}>
                  <TouchableHighlight
                    activeOpacity={0.9}
                    underlayColor="#FFF"
                    onPress={() => { this.updateStoreType(storeType); }}
                  >
                    {this.renderStoreType(storeType)}
                  </TouchableHighlight>
               </View>
              ))}
            </ScrollView>
            { Platform.OS === 'android' && this.renderSearchButton() }
          </Footer>
        </Animated.View>
      </View>);
  }
}

function mapStateToProps(state) {
  return {
    currentStoreType: state.currentStoreType,
    currentLocation: state.currentLocation,
    showsUserLocation: state.showsUserLocation
   };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Slider);
