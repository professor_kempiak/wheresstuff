import { Dimensions, StyleSheet } from 'react-native';

const SliderStyle = StyleSheet.create({
  animatedView: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    overflow: 'hidden',
  },
  slider: {
    flex: 1,
    position: 'absolute',
    zIndex: 10,
    left: 0,
    right: 0,
    bottom: 30
  },
  buttonText: {
    color: '#FFFFFF',
    fontWeight: 'bold'
  },
  keyboardBar: {
    backgroundColor: '#eee',
    position: 'absolute',
    left: 0,
    right: 0,
    alignItems: 'flex-end',
    padding: 3,
    paddingRight: 10
  },
  keyboardBarButtonText: {
    fontSize: 15,
    color: '#ED5565',
    fontWeight: 'bold'
  },
  searchInput: {
    height: 40,
    borderWidth: 0,
    backgroundColor: '#eee',
    color: '#333',
    paddingLeft: 10,
    paddingRight: 10,
    borderRadius: 20,
    margin: 10,
    marginTop: 20
  },
  title: {
    borderBottomWidth: 1,
    borderBottomColor: '#CCCCCC',
    marginTop: 20,
    paddingLeft: 10
  },
  titleText: {
    fontWeight: 'bold',
    color: '#aaa'
  },
  storeTypePicture: {
    width: 60,
    height: 73,
    margin: 10
  },
  storeTypeFocused: {
    opacity: 0.3
  },
  storeTypeNotFocused: {
    opacity: 0.8
  }
});

const NativeStyle = {
  buttonSearch: {
    backgroundColor: '#ED5565',
    width: 120,
    marginLeft: -60,
    left: Dimensions.get('window').width / 2,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 10,
  },
  footer: {
    flex: 1,
    backgroundColor: '#FFF',
    flexDirection: 'column'
  }
};

export { SliderStyle, NativeStyle };
