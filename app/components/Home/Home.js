import React, { Component } from 'react';
import ReactNative from 'react-native';
import { connect } from 'react-redux';
import { Container, Header } from 'native-base';

import Map from './Map/Map';
import Slider from './Slider/Slider';


const { View } = ReactNative;

class Home extends Component {

  render() {
    return (<Container>
          <View style={{ flex: 1 }}>
            <Map {...this.props} />
            <Slider {...this.props} />
          </View>
      </Container>);
  }
}

export default connect(() => { return {}; })(Home);
