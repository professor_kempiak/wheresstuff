import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Scene, Router } from 'react-native-router-flux';
import { Platform, StatusBar } from 'react-native';

import Home from './Home/Home';
import Detail from './Detail/Detail';


class AppContainer extends Component {

  componentDidMount() {
    StatusBar.setBarStyle('dark-content', true);
  }

  render() {
    return (
      <Router>
        <Scene
          //hideNavBar
          key="root"
          //'#1e2226'
          navigationBarStyle={{ backgroundColor: '#FFFFFF', borderBottomColor: '#ED5565', marginTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight }}
          barButtonIconStyle={{ tintColor: '#ED5565' }}
          titleStyle={{ color: '#ED5565' }}
        >
          <Scene key="home" component={Home} title="Interests"  initial />
          <Scene key="detail" component={Detail} title="Detail" />
        </Scene>
      </Router>
    );
  }
}

export default connect(() => { return {}; })(AppContainer);
