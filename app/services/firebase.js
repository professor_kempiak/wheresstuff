import * as firebase from 'firebase';

firebase.initializeApp({
    apiKey: "AIzaSyASJRXiFFTgvpFjayfhYg54YlYn8j9WXbA",
    authDomain: "poi-app-e7c30.firebaseapp.com",
    databaseURL: "https://poi-app-e7c30.firebaseio.com",
    projectId: "poi-app-e7c30",
    storageBucket: "poi-app-e7c30.appspot.com",
    messagingSenderId: "941553441374"
})

export const dbRef = firebase.database().ref();

export const setListener = (endpoint, updaterFn) => {
  firebase.database().ref(endpoint).on('value', updaterFn);
  return () => firebase.database().ref(endpoint).off();
}

export const pushData = (endpoint, data) => {
  return firebase.database().ref(endpoint).push(data);
}

export const getDefaultLocation = (updateDefaultLocation) => {
  dbRef.child('/locations').orderByChild('default').equalTo(true).once('value').then(function (snapshot) {
    const nodes = [];
    snapshot.forEach(child => { nodes.push({ ...child.val() }); });
    if (nodes.length > 0) {
      updateDefaultLocation(nodes[0].address);
    } else {
      updateDefaultLocation('New York');
    }
  }).catch((ex) => {
    console.log(ex);
    updateDefaultLocation('New York');
  });
}

export const login = (email, pass) =>
  firebase.auth()
    .signInWithEmailAndPassword(email, pass)

export const logout = () =>
  firebase.auth().signOut()

export const signup = (email, pass) =>
  firebase.auth().createUserWithEmailAndPassword(email, pass);
