import { signup, login, getDefaultLocation } from './firebase';

export {
  login,
  signup,
  getDefaultLocation
}

export const getMessages = (updaterFn) => setListener('messages', updaterFn);

export const postMessage = (message) => {
  if (Boolean(message)) {
    pushData('messages', {
      incoming: false,
      message
    })
  }
}
