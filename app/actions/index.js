import * as StoreActions from './stores';
import * as GeocoderActions from './geocoder';

export const ActionCreators = Object.assign({},
  StoreActions,
  GeocoderActions
);
