import Api from '../lib/api';
import * as types from './types';
import { googleApiKey } from '../Config';
import { dbRef } from "../services/firebase";

// const url = 'https://maps.googleapis.com/maps/api/place';
//
// function urlFor(service, params) {
//   return `${url}/${service}/json?${params.join('&')}`;
// }

export const searchStores = (coords, type) => {
  return (dispatch) => {
    // const params = [
    //   `location=${coords.latitude},${coords.longitude}`,
    //   'radius=10000',
    //   `type=${type}`,
    //   `key=${googleApiKey}`
    // ];
    // return Api.get(urlFor('radarsearch', params)).then(resp => {
    //   dispatch({ type: types.SET_FOUND_STORES, stores: resp.results });
    // }).catch((ex) => {
    //   console.log(ex);
    //   dispatch({
    //     type: types.ERROR,
    //     message: 'Error while call google API please ask for support.'
    //   });
    // });

    return dbRef.child('places/' + coords.address).orderByChild('types').equalTo(type).once('value').then(function (snapshot) {
      const nodes = [];

      snapshot.forEach(child => { nodes.push({ ...child.val(), locAddress: coords.address }); });

      if (nodes.length > 0) {
        return dispatch({ type: types.SET_FOUND_STORES, stores: nodes });
      } else {
        return dispatch({
          type: types.ERROR,
          message: 'Error while call google API please ask for support.'
        });
      }
    }).catch((ex) => {
      console.log(ex);
      return dispatch({
        type: types.ERROR,
        message: 'Error while call google API please ask for support.'
      });
    });
  };
};

export const findStore = (id, address) => {
  return (dispatch) => {
    // const params = [`placeid=${id}`, `key=${googleApiKey}`, 'language=en', 'region=US'];
    // return Api.get(urlFor('details', params)).then(resp => {
    //   dispatch({ type: types.SET_STORE_DETAIL, detail: resp.result });
    // }).catch((ex) => {
    //   console.log(ex);
    //   dispatch({
    //     type: types.ERROR,
    //     message: 'Error while call google API please ask for support.'
    //   });
    // });

    return dbRef.child('places/' + address).orderByChild('placeId').equalTo(id).once('value').then(function (snapshot) {
      const nodes = [];

      snapshot.forEach(child => { nodes.push({ ...child.val() }); });

      return dispatch({ type: types.SET_STORE_DETAIL, detail: nodes[0] });
    }).catch((ex) => {
      console.log(ex);
      return dispatch({
        type: types.ERROR,
        message: 'Error while call google API please ask for support.'
      });
    });
  };
};

export const changeStoreType = (storeTypeSelected) => {
  return {
    type: types.CHANGE_STORE_TYPE,
    storeType: storeTypeSelected
  };
};

export const changeShowUserLocation = (show) => {
  return {
    type: types.SHOW_USER_LOCATION,
    showUserLocation: show
  };
};

export const changeLocation = (newLocation) => {
  return {
    type: types.CHANGE_LOCATION,
    location: newLocation
  };
};
