import Api from '../lib/api';
import * as types from './types';
import * as StoreActions from './stores';
import { googleApiKey } from '../Config';
import { dbRef } from "../services/firebase";

export const findStoreFromAddress = (address, storeType) => {
  return (dispatch) => {
    return dbRef.child('/locations').orderByChild('address').startAt(address).once('value').then(function (snapshot) {
      const nodes = [];

      snapshot.forEach(child => { nodes.push({ ...child.val(), key: child.key }); });

      if (nodes.length > 0) {
         const foundLocation = {
             address: nodes[0].address,
             latitude: nodes[0].latitude,
             longitude: nodes[0].longitude
         };

        return dispatch(StoreActions.changeLocation(foundLocation))
         && dispatch(StoreActions.changeShowUserLocation(false))
         && dispatch(StoreActions.searchStores(foundLocation, storeType.ref));
      } else {
        return dispatch({
          type: types.ERROR,
          message: 'Error while call google API please ask for support.'
        });
      }
    }).catch((ex) => {
      console.log(ex);
      return dispatch({
        type: types.ERROR,
        message: 'Error while call google API please ask for support.'
      });
    });
  };
};
