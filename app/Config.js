import pointOfInterestPins from './assets/point_of_interest_pins2.png';
import parkingPins from './assets/parking_pins2.png';
import bankPins from './assets/bank_pins2.png';
import pharmacyPins from './assets/pharmacy_pins2.png';
import cafePins from './assets/cafe_pins2.png';
import storePins from './assets/store_pins2.png';

import filterPointOfInterest from './assets/point_of_interest_filter.png';
import filterParking from './assets/parking_filter.png';
import filterBank from './assets/bank_filter.png';
import filterPharmacy from './assets/pharmacy_filter.png';
import filterCafe from './assets/cafe_filter.png';
import filterStore from './assets/store_filter.png';

const storeTypes = [
  { ref: 'local_government_office', pins: pointOfInterestPins, filterImg: filterPointOfInterest },
  { ref: 'parking', pins: parkingPins, filterImg: filterParking },
  { ref: 'bank', pins: bankPins, filterImg: filterBank },
  { ref: 'pharmacy', pins: pharmacyPins, filterImg: filterPharmacy },
  { ref: 'cafe', pins: cafePins, filterImg: filterCafe },
  { ref: 'store', pins: storePins, filterImg: filterStore }
];
const defaultStoreType = storeTypes[0];

const googleApiKey = 'AIzaSyDWhXWiA_4EQu4Ldk4W8jpq58R-Z3dr-fk';

export { storeTypes, defaultStoreType, googleApiKey };
