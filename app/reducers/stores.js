import createReducer from '../lib/createReducer';
import * as types from '../actions/types';

import { defaultStoreType } from '../Config';


export const foundStores = createReducer([], {
  [types.SET_FOUND_STORES](state, action) {
    return action.stores.slice(0, 10).map((store) => (
       {
        placeId: store.placeId,
        locAddress : store.locAddress,
        location: {
          latitude: store.location.latitude,
          longitude: store.location.longitude
        }
      }
    ));
  }
});

export const storeDetail = createReducer({}, {
  [types.SET_STORE_DETAIL](state, action) {
    return action.detail;
  }
});

export const currentStoreType = createReducer(defaultStoreType, {
  [types.CHANGE_STORE_TYPE](state, action) {
    return action.storeType;
  }
});

export const currentLocation = createReducer({ latitude: 0, longitude: 0 }, {
  [types.CHANGE_LOCATION](state, action) {
    return action.location;
  }
});

export const showUserLocation = createReducer(true, {
  [types.SHOW_USER_LOCATION](state, action) {
    return action.showUserLocation;
  }
});

export const error = createReducer(null, {
  [types.ERROR](state, action) {
    return action.message;
  }
});
