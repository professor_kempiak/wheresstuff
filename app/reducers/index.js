 import { combineReducers } from 'redux';
 import * as storesReducer from './stores';

 export default combineReducers(Object.assign(
  storesReducer
));
