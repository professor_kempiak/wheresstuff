class Api {
  static headers() {
    return {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      dataType: 'json',
    };
  }

  static get(url) {
    return this.xhr(url, null, 'GET');
  }

  static put(url, params) {
    return this.xhr(url, params, 'PUT');
  }

  static post(url, params) {
    return this.xhr(url, params, 'POST');
  }

  static delete(url, params) {
    return this.xhr(url, params, 'DELETE');
  }

  static xhr(url, params, verb) {
    const options = Object.assign({
                      method: verb
                    }, params ? { body: JSON.stringify(params) } : null
                  );
    options.headers = Api.headers();
    return fetch(url, options).then(resp => {
      const json = resp.json();
      if (resp.ok) {
        return json;
      }
      return json.then(err => { throw err; });
    }).then(json => json);
  }
}
export default Api;
