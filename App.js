import React from 'react'
import { StyleSheet, Platform, Image, Text, View } from 'react-native'
import { SwitchNavigator } from 'react-navigation'

import Login from './app/components/Login';
import SignUp from './app/components/SignUp';
import Main from './app/components/Main';

// create our app's navigation stack
const App = SwitchNavigator(
  {
    Login,
    SignUp,
    Main
  },
  {
    initialRouteName: 'Login'
  }
)

export default App